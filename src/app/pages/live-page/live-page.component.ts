import { Component, ViewChild } from '@angular/core';
import { DummyFormComponent } from '../../components/dummy-form/dummy-form.component';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { HotToastService, Toast, provideHotToastConfig } from '@ngneat/hot-toast';

@Component({
  selector: 'app-live-page',
  standalone: true,
  imports: [
    CommonModule,
    DummyFormComponent,
    FormsModule
  ],
  templateUrl: './live-page.component.html',
  styleUrl: './live-page.component.scss'
})
export class LivePageComponent {
  @ViewChild("PARENT_FORM")
  private form!: NgForm;

  clickOnSubmit = false;
  calledSubmit = false;

  constructor(
    private toast: HotToastService
  ) { }

  onSubmit(event: any) {
    console.log('form', this.form.form)
    console.log('this is status', this.form.form.valid)
    console.warn('event', event)
  }



}
