import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';

@Component({
  selector: 'app-dummy-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
  ],
  templateUrl: './dummy-form.component.html',
  styleUrl: './dummy-form.component.scss'
})
export class DummyFormComponent {

  @ViewChild("FORM_DATA")
  private form!: NgForm;
  @Input() clickOnSubmit = false;
  @Output() clickOnSubmitChange = new EventEmitter<boolean>();
  @Output() onSubmit = new EventEmitter();

  formData: any = {}

  ngOnChanges(change: SimpleChanges) {
    if (change["clickOnSubmit"] && this.clickOnSubmit) {
      this.submitForm()
    }
  }

  submitForm() {
    let formControls = this.form.form.controls
    for (let control in formControls) {
      formControls[control].markAllAsTouched();
      formControls[control].markAsDirty();
    }
    this.clickOnSubmit = false;
    setTimeout(() => this.clickOnSubmitChange.emit(false))
    try {
      console.log('this.form.form.valid', this.form.form.valid)
      if (this.form.form.valid) {
        this.onSubmit.emit({
          form: this.form
        })
      }
    } catch { }
  }

}
